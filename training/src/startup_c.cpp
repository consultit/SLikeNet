/**
 * startup_c.cpp
 *
 *  Created on: Apr 30, 2019
 *      Author: consultit
 */

#include "common.h"

using namespace RakNet;

int main(int argc, char **argv)
{
	// CLIENT
	// Instancing
	RakPeerInterface* peer = RakNet::RakPeerInterface::GetInstance();
	// Connection as Client
	SocketDescriptor sd;
	peer->Startup(1, &sd, 1);
	peer->Connect(SERVER_IP, SERVER_PORT, 0, 0);
	return 0;
}


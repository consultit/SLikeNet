/**
 * common.h
 *
 *  Created on: Jun 2, 2019
 *      Author: consultit
 */

#ifndef TRAINING_SRC_COMMON_H_
#define TRAINING_SRC_COMMON_H_

#include "RakPeer.h"
#include "MessageIdentifiers.h"

namespace RakNet = SLNet;

constexpr char* SERVER_IP = "localhost";
constexpr unsigned short SERVER_PORT = 61000;

inline unsigned char GetPacketIdentifier(RakNet::Packet *p)
{
	if ((unsigned char) p->data[0] == ID_TIMESTAMP)
	{
		return (unsigned char) p->data[sizeof(RakNet::MessageID)
				+ sizeof(RakNet::Time)];
	}
	else
	{
		return (unsigned char) p->data[0];
	}
}

#endif /* TRAINING_SRC_COMMON_H_ */
